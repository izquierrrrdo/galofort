$(function() {

	$(".room__point__btn").on({
		mouseenter: function () {
			$('.wow').removeClass('wow');
			$('.slide__bg').addClass('wow bounceInUp');

			if (!($(this).parent().children('.room__point__inner').is(":visible"))) {
				$('.room__point__inner').hide();
				$(this).parent().children('.room__point__inner').show();
				$(this).parent().children('.room__point__inner').addClass('wow fadeIn');
				$(this).parent().children('.room__point__inner').children().addClass('wow fadeInLeft');
				new WOW().init();
			}
		}
	});

	$(".room__point").on({
		mouseleave: function () {
			window.setTimeout(function(){
				var hovered = $("#rooms").find(".room__point:hover").length;
				if (!hovered) {
					$(".room__point__inner").fadeOut();
				}
			}, 500);
		}
	});

	//добавление классов анимации для секции svg иконок
	function addAnim(){
		$('.st3__img svg path, .st3__img svg circle').each(function(){
			var id = $(this).attr('id');
			if(id) $(this).addClass(id)

		})
	}

	// offset для секции с svg иконками
	var sectionOffset =  $(".st3").offset().top;
	if($(this).scrollTop()>sectionOffset) {
		addAnim();
	}

	// if($(this).scrollTop()>sectionOffset+600) {
	// 	$('.st3').addClass('blue');
	// 	$('.sect4__part1').addClass('blue');
	// }
	$(window).scroll(function() {
		if($(this).scrollTop()>sectionOffset) {
			addAnim();
		}

		// if($(this).scrollTop()>sectionOffset+600) {
		// 	$('.st3').addClass('blue');
		// 	$('.sect4__part1').addClass('blue');
		// } else {
		// 	$('.st3').removeClass('blue');
		// }

	});

	$('.room__arrow').click(function(){
		$('.sect4__slider').slick('slickNext');
	})

	$('.room__backarr').click(function(){
		$('.sect4__slider').slick('slickPrev');
	})

	$('.sect4__slider').slick({
		infinite: false,
		arrows: false,
		speed: 1800
	});

	$('.sect4__slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		$('#dr').animate({
			width: '100',
		}, 1000, 'linear');

		$('#dr1').animate({
			x: '100',
			width: '10',
		}, 800, 'linear');

		$('#knob').animate({
			opacity: '0',
		}, 500);

		$('#knob1').animate({
			opacity: '0',
		}, 500);

		$('#knob2').animate({
			cx: '96',
		}, 800);

		$('#SVGID_6_').animate({
			cx: '101',
		}, 2000);

	});

	$('.qsitem__title, .qsitem__icon').click(function(){
		var parent = $(this).closest('.qsitem');
		var el = parent.find('.qsitem__txt');
		parent.toggleClass('open');
		if(parent.hasClass('open')) parent.find('.qsitem__icon img').attr('src', 'img/design/qs_top.svg')
		else parent.find('.qsitem__icon img').attr('src', 'img/design/qs_bottom.svg')
		// parent.find('.qsitem__icon img').toggle( "slide" );
		el.slideToggle();
	})

	$('.menu__icon').click(function(){
		$('.topMenu__open').slideToggle();
		$('.topMenu__open ul').fadeToggle(500);
		$('.topMenu__bg').fadeToggle();
		$(this).toggleClass('open');
	})

	menuHide = function() {
		$('.topMenu__open').hide();
		$('.topMenu__open ul').hide();
		$('.topMenu__bg').hide();
	}

	$(document).click(function(event) {
		if (!$(event.target).closest(".menu__icon, .topMenu__open, .topMenu__open ul ").length) {
			menuHide();
		}
	});

	$('.topMenu__open a').click(function(){
		menuHide();
	})

	// Select all links with hashes
	$('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]')
	.not('[href="#0"]')
	.click(function(event) {
	// On-page links
	if (
		location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
		&&
		location.hostname == this.hostname
	) {
		// Figure out element to scroll to
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		// Does a scroll target exist?
		if (target.length) {
		// Only prevent default if animation is actually gonna happen
		event.preventDefault();
		$('html, body').animate({
			scrollTop: target.offset().top
		}, 1000, function() {
			// Callback after animation
			// Must change focus!
			var $target = $(target);
			$target.focus();
			if ($target.is(":focus")) { // Checking if the target was focused
			return false;
			} else {
			$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
			$target.focus(); // Set focus again
			};
		});
		}
	}
	});


	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		}
	});

	//counter

	$('.counter').counterUp({
		 delay: 100,
		 time: 1500
	});


	//timer

	function counter(el) {
		var count = +el.text();
			if(count<=10) {
				/*if(count == 0) {
					el.text(59);
					return;
				}*/
				el.text('0'+(count+1));
			}  else {
				if(count == 59) {
					el.text('00');
					return
				}
				el.text(count+1);
			}
	}
	var secCount = $('.sec');
	var minCount = $('.min');

	var timerSec = setInterval(function() {
			counter(secCount);
		},1000);

	var timerMin = setInterval(function() {
		counter(minCount);
	},60000);

	var secCount1= $('.sec1');
	var minCount1 = $('.min1');

	var timerSec1 = setInterval(function() {
			counter(secCount1);
	},1000);

	var timerMin1 = setInterval(function() {
		counter(minCount1);
	},60000);


	//wow

	new WOW().init();

	//jarallax

	 // jarallax(document.querySelectorAll('.jarallax'));

	//draw snow

	/*
	var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");

	//canvas dimensions
	var W = window.innerWidth;
	var H = window.innerHeight;
	canvas.width = W;
	canvas.height = H;

	//snowflake particles
	var mp = 25; //max particles
	var particles = [];
	for(var i = 0; i < mp; i++)
	{
		particles.push({
			x: Math.random()*W, //x-coordinate
			y: Math.random()*H, //y-coordinate
			r: Math.random()*4+1, //radius
			d: Math.random()*mp //density
		})
	}

	//Lets draw the flakes
	//canvas init
	var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");

	//canvas dimensions
	var W = window.innerWidth;
	var H = window.innerHeight;
	canvas.width = W;
	canvas.height = H;

	//snowflake particles
	var mp = 85; //max particles
	var particles = [];
	for(var i = 0; i < mp; i++)
	{
		particles.push({
			x: Math.random()*W, //x-coordinate
			y: Math.random()*H, //y-coordinate
			r: Math.random()*4+1, //radius
			d: Math.random()*mp //density
		})
	}

	function draw()
	{
		ctx.clearRect(0, 0, W, H);

		ctx.fillStyle = "rgba(255, 255, 255, 0.8)";
		ctx.beginPath();
		for(var i = 0; i < mp; i++)
		{
			var p = particles[i];
			ctx.moveTo(p.x, p.y);
			ctx.arc(p.x, p.y, p.r, 0, Math.PI*2, true);
		}
		ctx.fill();
	}

	draw();

	*/
	var padding = ($(window).width()-1170)/2+'px';

	//slider
	window.addEventListener('resize', function(){
		if($( window ).width()>768) {
			padding = ($(window).width()-1170)/2+'px';
			$('.slider').slick('slickSetOption', 'centerPadding', padding);
			//$('.slider').slick('slickSetOption', 'centerMode', true);
		} else {
			//$('.slider').slick('slickSetOption', 'centerMode', false, false);
			$('.slider').slick('slickSetOption', 'centerPadding',0);
		}
	})

	$('.slider').slick({
		centerMode: true,
		centerPadding: padding,
		infinite: false,
		arrows: false,
		/*responsive: [
			{
				breakpoint: 768,
				settings: {
					centerMode: false,
					centerPadding: 0
				}
			}]*/
	});

	$('.slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
		$('.slider').slick('slickSetOption', 'infinite', true, true);
	});

	$('.slide__next').click(function(){
		// $('.wow').removeClass('.wow');
		// $('.slider').slick('slickNext');
		// setTimeout(function(){
		// 	$('.slide__bg').addClass('wow bounceInUp');
		// 	new WOW().init();
		// }, 50);

		$('.slider').slick('slickNext');
		new WOW().init();




	});

	$('.actionBtn__ts1').click(function(){
		$('.feedback__item').slideDown();
	});

	$('.md-hide').click(function(){
		$('.md-show').show();
	});

	$(window).on('load', function () {
		$('.slider').css('opacity', '1')
	});

	$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		removalDelay: 500,
		callbacks: {
			beforeOpen: function() {
				$('.header').css('top', '0')
			},
			 close: function() {
				$('.header').css('top', '60px')
			  },
		  },
	});
});
jQuery(document).ready(function($) {
  setTimeout(function() {
    $('.preloader').fadeOut('slow');
  }, 200);

  var canvas = document.querySelector('.snow'),
    ctx = canvas.getContext('2d'),
    windowW = window.innerWidth,
    windowH = 1200,
    numFlakes = 110,
    flakes = [];

function Flake(x, y) {
  var maxWeight = 1,
      maxSpeed = 0.02;

  this.x = x;
  this.y = y;
  this.r = randomBetween(0, 0.02);
  this.a = randomBetween(0, Math.PI*5);
  this.aStep = 0.01;

  this.weight = 2;
  this.height = 2;
  this.alpha = 1;
  this.speed = (this.weight / maxWeight) * maxSpeed;

  this.update = function() {
    this.x += (Math.cos(this.a) * this.r)/3;
    this.a += this.aStep;
	this.y += (Math.sin(this.a) * this.r)/3;
  }

}

function init() {
  var i = numFlakes,
      flake,
      x,
      y;

  while (i--) {
    x = randomBetween(0, windowW, true);
    y = randomBetween(0, windowH, true);


    flake = new Flake(x, y);
    flakes.push(flake);
  }

  scaleCanvas();
  loop();
}

function scaleCanvas() {
  canvas.width = windowW;
  canvas.height = windowH;
}

function loop() {
  var i = flakes.length,
      z,
      dist,
      flakeA,
      flakeB;

  // clear canvas
  ctx.save();
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  ctx.clearRect(0, 0, windowW, windowH);
  ctx.restore();

  // loop of hell
  while (i--) {

    flakeA = flakes[i];
    flakeA.update();


    /*for (z = 0; z < flakes.length; z++) {
      flakeB = flakes[z];
      if (flakeA !== flakeB && distanceBetween(flakeA, flakeB) < 150) {
        ctx.beginPath();
        ctx.moveTo(flakeA.x, flakeA.y);
        ctx.lineTo(flakeB.x, flakeB.y);
        ctx.strokeStyle = '#444444';
        ctx.stroke();
        ctx.closePath();
      }
    }*/


    ctx.beginPath();
    ctx.arc(flakeA.x, flakeA.y, flakeA.weight, 0, 2 * Math.PI, false);
    ctx.fillStyle = 'rgb(255, 255, 255)';
    ctx.fill();

    // if (flakeA.y >= windowH) {
    //   flakeA.y = -flakeA.weight;
    // }
  }

  requestAnimationFrame(loop);
}

function randomBetween(min, max, round) {
  var num = Math.random() * (max - min + 1) + min;

  if (round) {
    return Math.floor(num);
  } else {
    return num;
  }
}

function distanceBetween(vector1, vector2) {
  var dx = vector2.x - vector1.x,
      dy = vector2.y - vector1.y;

  return Math.sqrt(dx*dx + dy*dy);
}

init();


//scrollMagic
var controller = new ScrollMagic.Controller();
// var ourScene = new ScrollMagic.Scene({
// 	triggerElement: '#ice1'
// }).setClassToggle("#ice1", 'enlarge')
// .addTo(controller);

// var ourScene4 = new ScrollMagic.Scene({
// 	triggerElement: '#ice1'
// }).setClassToggle("#ice1", 'iceone')
// .addTo(controller);


var ourScene3 = new ScrollMagic.Scene({
	triggerElement: '#zabolevanie'
}).setClassToggle(".range__value", 'animate')
.addTo(controller);

// var ourScene2 = new ScrollMagic.Scene({
// 	triggerElement: '#ice1'
// }).setClassToggle("#ice2", 'fade-in')
// .addTo(controller);
});



