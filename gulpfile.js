var gulp           = require('gulp'),
		gutil          = require('gulp-util' ),
		sass           = require('gulp-sass'),
		browserSync    = require('browser-sync'),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify'),
		cleanCSS       = require('gulp-clean-css'),
		rename         = require('gulp-rename'),
		del            = require('del'),
		imagemin       = require('gulp-imagemin'),
		cache          = require('gulp-cache'),
		autoprefixer   = require('gulp-autoprefixer'),
		ftp            = require('vinyl-ftp'),
		notify         = require("gulp-notify");

// Скрипты проекта

gulp.task('common-js', function() {
	return gulp.src([
		'assets/template/app/js/common.js',
		])
	.pipe(gulp.dest('assets/template/app/js'));
});


gulp.task('scripts', function() {
	return gulp.src([
		'assets/template/app/libs/jquery_2.2.4/jquery-2.2.js',
		'assets/template/app/libs/mmenu/js/jquery.mmenu.all.min.js',
		'assets/template/app/libs/jquery-ui/jquery-ui.min.js',
		'assets/template/app/libs/counter/jquery.counterup.min.js',
		'assets/template/app/libs/counter/waypoints.min.js',
		//'assets/template/app/libs/fotorama/fotorama.js',
		'assets/template/app/libs/slick/slick.min.js',
		//'assets/template/app/libs/selectize/js/standalone/selectize.min.js',
		//'assets/template/app/libs/equalHeights/equalheights.js',
		'assets/template/app/libs/magnific-popup/jquery.magnific-popup.js',
		//'assets/template/app/libs/simplebar/simplebar.js',
		'assets/template/app/libs/wow/wow.js',
		'assets/template/app/libs/jarallax/jarallax.min.js',
		'assets/template/app/libs/jarallax/jarallax-element.min.js',
		'assets/template/app/libs/scrollmagic/ScrollMagic.min.js'
		])
	.pipe(concat('scripts.min.js'))
	// .pipe(uglify()) // Минимизировать весь js (на выбор)
	.pipe(gulp.dest('assets/template/app/js'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'assets/template/app'
		},
		notify: false,
		// tunnel: true,
		// tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
	});
});

gulp.task('sass', function() {
	return gulp.src('assets/template/app/sass/**/*.sass')
	.pipe(sass().on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleanCSS())
	.pipe(gulp.dest('assets/template/app/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['sass', 'scripts', 'browser-sync'], function() {
	gulp.watch('assets/template/app/sass/**/*.sass', ['sass']);
	gulp.watch(['libs/**/*.js', 'assets/template/app/js/common.js'], ['scripts']);
	gulp.watch('assets/template/app/*.html', browserSync.reload);
});

gulp.task('imagemin', function() {
	return gulp.src('assets/template/app/img/**/*')
	.pipe(cache(imagemin()))
	.pipe(gulp.dest('dist/img')); 
});

gulp.task('build', ['removedist', 'imagemin', 'sass', 'common-js', 'scripts'], function() {

	var buildFiles = gulp.src([
		'assets/template/app/*.html',
		'assets/template/app/.htaccess',
		]).pipe(gulp.dest('dist'));

	var buildCss = gulp.src([
		'assets/template/app/css/main.min.css',
		'assets/template/app/css/svg.css',
		]).pipe(gulp.dest('dist/css'));
		
	var buildJs = gulp.src([
		'assets/template/app/js/common.js',
		]).pipe(gulp.dest('dist/js'));

	var buildJs = gulp.src([
		'assets/template/app/js/scripts.min.js',
		]).pipe(gulp.dest('dist/js'));

	var buildFonts = gulp.src([
		'assets/template/app/fonts/**/*',
		]).pipe(gulp.dest('dist/fonts'));

});

gulp.task('deploy', function() {

	var conn = ftp.create({
		host:      'hostname.com',
		user:      'username',
		password:  'userpassword',
		parallel:  10,
		log: gutil.log
	});

	var globs = [
	'dist/**',
	'dist/.htaccess',
	];
	return gulp.src(globs, {buffer: false})
	.pipe(conn.dest('/path/to/folder/on/server'));

});

gulp.task('removedist', function() { return del.sync('dist'); });
gulp.task('clearcache', function () { return cache.clearAll(); });

gulp.task('default', ['watch']);
